/* See LICENSE file for copyright and license details. */

#define XF86AudioMedia 0x1008ff32
#define Print 0xff61
#define XF86AudioMute 0x1008ff12
#define XF86AudioLowerVolume 0x1008ff11
#define XF86AudioRaiseVolume 0x1008ff13
#define XF86XK_PowerOff 0x1008ff2a
#define Pause 0xff13
#define XF86AudioPrev 0x1008ff16
#define XF86AudioPlay 0x1008ff14
#define XF86AudioNext 0x1008ff17
#define Grave 0x60
#define Meta 0xffeb

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappx     = 15;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int vertpad            = 16;       /* vertical padding of bar */
static const int sidepad            = 16;       /* horizontal padding of bar */
static const char *fonts[]          = { "BlexMono Nerd Font Mono:size=12" };
static const char dmenufont[]       = "BlexMono Nerd Font Mono:size=12";
static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#009999";
static const char col_gray3[]       = "#00ffff";
static const char col_gray4[]       = "#555555";
static const char col_cyan[]        = "#ff00ff";
static const unsigned int baralpha = 0xcc;
static const unsigned int borderalpha = 0;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	// [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	// [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
	[SchemeNorm] = { col_gray2, col_gray1, col_gray1 },
	[SchemeSel]  = { col_cyan, col_gray1,  col_cyan  },
	[SchemeHid]  = { col_gray4,  col_gray1, col_cyan  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
	[SchemeHid]  = { baralpha, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "󰚄", "󰟟", "󰉊", "", "󰊠", "󰍳", "󰂓", "", "" };
// static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
static const int mainmon = 0; /* xsetroot will only change the bar on this monitor */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]",      tile },    /* first entry is default */
	{ "[]",      NULL },    /* no layout function means floating behavior */
	{ "[󰛐]",      monocle },
	// { "[]=",      tile },    /* first entry is default */
	// { "><>",      NULL },    /* no layout function means floating behavior */
	// { "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ KeyPress, MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ KeyPress, MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ KeyPress, MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ KeyPress, MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray2, "-sb", col_gray1, "-sf", col_cyan, NULL };
static const char *const termcmd  = "st -e bash -c 'tmux attach-session -t 󰛸 || exec tmux new-session -s 󰛸 && exit'";

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ KeyPress, MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ KeyPress, MODKEY|ShiftMask,             XK_Return, spawn,          SHCMD(termcmd) },
	{ KeyPress, MODKEY,                       XK_b,      togglebar,      {0} },
	// { KeyPress, MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	// { KeyPress, MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ KeyPress, MODKEY,                       XK_j,      focusstackvis,  {.i = +1 } },
	{ KeyPress, MODKEY,                       XK_k,      focusstackvis,  {.i = -1 } },
	{ KeyPress, MODKEY|ShiftMask,             XK_j,      focusstackhid,  {.i = +1 } },
	{ KeyPress, MODKEY|ShiftMask,             XK_k,      focusstackhid,  {.i = -1 } },
	{ KeyPress, MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ KeyPress, MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ KeyPress, MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ KeyPress, MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ KeyPress, MODKEY,                       XK_Return, zoom,           {0} },
	{ KeyPress, MODKEY,                       XK_Tab,    view,           {0} },
	{ KeyPress, MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ KeyPress, MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ KeyPress, MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ KeyPress, MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ KeyPress, MODKEY,                       XK_space,  setlayout,      {0} },
	{ KeyPress, MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ KeyPress, MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ KeyPress, MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ KeyPress, MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ KeyPress, MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ KeyPress, MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ KeyPress, MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ KeyPress, MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ KeyPress, MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ KeyPress, MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	{ KeyPress, 0,              Meta,  showFocusBorder,        {.i = 1} },
	{ KeyRelease, MODKEY,              Meta,  showFocusBorder,        {.i = 0} },
	{ KeyPress, MODKEY,                       XK_s,      show,           {0} },
	{ KeyPress, MODKEY|ShiftMask,             XK_s,      showall,        {0} },
	{ KeyPress, MODKEY,                       XK_g,      hide,           {0} },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ KeyPress, MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ KeyPress, MODKEY|ShiftMask,             XK_l,      spawn,           SHCMD("~/.custom/./lock.sh") },
	{ KeyPress, MODKEY,             XK_r,      spawn,           SHCMD("~/.custom/./sfeed-dmenu.sh") },
	{ KeyPress, 0,             XF86AudioMedia,      spawn,           SHCMD("~/.custom/./brightness-up.sh") },
	{ KeyPress, ShiftMask,             XF86AudioMedia,      spawn,           SHCMD("~/.custom/./brightness-dn.sh") },
	{ KeyPress, 0,             XF86AudioMute,      spawn,           SHCMD("~/.custom/./mute.sh") },
	{ KeyPress, ShiftMask,             XF86AudioMute,      spawn,           SHCMD("~/.custom/./unmute.sh") },
	{ KeyPress, 0,             XF86AudioRaiseVolume,      spawn,           SHCMD("~/.custom/./volup.sh") },
	{ KeyPress, 0,             XF86AudioLowerVolume,      spawn,           SHCMD("~/.custom/./voldn.sh") },
	{ KeyPress, ControlMask,             XF86AudioMedia,      spawn,           SHCMD("~/.custom/./wifioff.sh") },
	{ KeyPress, ControlMask|ShiftMask,             XF86AudioMedia,      spawn,           SHCMD("~/.custom/./wifion.sh") },
	{ KeyPress, MODKEY,             XF86AudioMedia,      spawn,           SHCMD("~/.custom/./ditouch.sh") },
	{ KeyPress, MODKEY|ShiftMask,             XF86AudioMedia,      spawn,           SHCMD("~/.custom/./entouch.sh") },
	{ KeyPress, 0,             Print,      spawn,           SHCMD("scrot ~/Pictures/Screenshots/%Y-%m-%d-%H-%M-%S.png -e 'xclip -selection clipboard -t image/png -i ~/Pictures/Screenshots/%Y-%m-%d-%H-%M-%S.png'") },
	{ KeyPress, 0, XF86XK_PowerOff,      spawn,      SHCMD("~/.custom/./powermenu.sh") },
	{ KeyPress, 0, Pause          ,      spawn,      SHCMD("~/.custom/./powermenu.sh") },
	{ KeyPress, MODKEY, XK_o,      spawn,      SHCMD("bash -c ~/.custom/./dmenu-file.sh &") },
	{ KeyPress, MODKEY,                       XK_n,      setlayout,      {.v = &layouts[1]} },
	{ KeyPress, MODKEY,             XK_n, spawn,           SHCMD(termcmd) },
	{ KeyPress, 0,             XF86AudioPrev, spawn,          SHCMD("playerctl previous") },
	{ KeyPress, 0,             XF86AudioPlay, spawn,          SHCMD("playerctl play-pause") },
	{ KeyPress, 0,             XF86AudioNext, spawn,          SHCMD("playerctl next") },
	{ KeyPress, MODKEY,             Grave, spawn,          SHCMD("presmouse --keybind") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button1,        togglewin,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          SHCMD(termcmd) },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
